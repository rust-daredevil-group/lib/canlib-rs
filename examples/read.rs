// Reading CAN, no CanStatus error checking.

extern crate canlib_rs;

use canlib_rs::*;

fn main() {
    can_initialize_library();

    let mut hnd: CanHandle;

    hnd = can_open_channel(0, CanOpen::Exclusive as i32);

    can_set_bus_params(
        hnd, 
        CanBitrate::_62K, 
        11, 
        4, 
        4, 
        1
    );

    can_set_bus_output_control(hnd, CanDriver::Normal);

    can_bus_on(hnd);

    let mut id: i32 = 0;
    let mut buffer = [0; 64];
    let mut dlc: u32 = 0;
    let mut flag: u32 = 0;
    let mut time: u32 = 0;
    let timeout: u32 = 5000;

    can_read_sync(hnd, timeout);

    can_read(
        hnd,
        &mut id,
        &mut buffer,
        &mut dlc,
        &mut flag,
        &mut time
    );

    for i in 0..dlc {
        print!("{}", buffer[i as usize]);
    }

    can_bus_off(hnd);
    can_close(hnd);
    can_unload_library();
}