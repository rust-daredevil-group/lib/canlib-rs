// Writing to CAN, with CanStatus simple error printing.

extern crate canlib_rs;

use canlib_rs::*;

fn check_error(status: CanStatus, func: String) {
    if status != CanStatus::CanOk {
        println!("{} Error: {}", func, status as i32);
    }
}

fn main() {
    // Initialize
    can_initialize_library();

    let mut hnd: CanHandle;

    hnd = can_open_channel(0, CanOpen::Exclusive as i32);

    let bus_stat = can_set_bus_params(
        hnd, 
        CanBitrate::_62K, 
        11, 
        4, 
        4, 
        1
    );
    check_error(bus_stat, String::from("can_set_bus_params"));

    let bus_out_stat = can_set_bus_output_control(hnd, CanDriver::Normal);
    check_error(bus_out_stat, String::from("can_set_bus_output_control"));

    let bus_on_stat = can_bus_on(hnd);
    check_error(bus_on_stat, String::from("can_bus_on"));

    // Create message
    let mut buffer = [0;64];
    let mut msg = String::from("Hello!");

    for i in msg.len()-1..=0 {
        buffer[i] = msg.pop().unwrap() as u8;
    }

    // Write message
    let write_stat = can_write(
        hnd,
        123,
        &mut buffer,
        msg.len() as u32,
        0
    );
    check_error(write_stat, String::from("can_write"));

    // Wait for 5 seconds or until message is sent
    let write_sync_stat = can_write_sync(hnd, 5000);
    check_error(write_sync_stat, String::from("can_write_sync"));

    // Close the channel and unload library
    can_bus_off(hnd);
    can_close(hnd);
    can_unload_library();
}