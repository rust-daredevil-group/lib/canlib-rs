# canlib-rs
Provides safe Rust wrappings to the CANlib library. Internally it uses unsafe 
auto-generated bindings on the CANlib header file. 

This is very barebones. Far from all features are supported.

## Prerequisites
You will need Kvaser's drivers and SDK. See:  
https://www.kvaser.com/download/

## Installation
Add the following to your dependencies:

```
// Link to repository
[dependencies]
canlib-rs = { git = "https://gitlab.com/rust-daredevil-group/canlib-rs.git" }
```
Otherwise you can just download it and add
```
// Link to local
[dependencies]
canlib-rs = { path = "/path/to/canlib-rs" }
```

## Usage


## Examples
Examples will be provided in the 'examples' sub-directory.

