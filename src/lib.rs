//! # canlib-rs
//! 
//! Provides safe wrappings around the CANlib C library.
//! Written on top of canlib-sys which provides unsafe auto-
//! generated bindings on the CANlib C header file. 
//! 
//! Read the official CANlib documentation prior to usage!
//! 

extern crate canlib_sys;
extern crate libc;

use canlib_sys::*;
use libc::{c_int, c_uint, c_long, c_ulong, c_void};

pub type CanHandle = i32;

/// CAN driver modes.
#[derive(Copy, Clone)]
pub enum CanDriver {
    Off = 0,
    Silent =  1,
    Normal = 4,
    SelfReception =  8,
}

/// CAN channel flags.
#[derive(Copy, Clone)]
pub enum CanOpen {
    Exclusive = 0x0008,
    RequireExtended = 0x0010,
    AcceptVirtual = 0x0020,
    OverrideExclusive = 0x0040,
    RequireInitAccess = 0x0080,
    NoInitAccess = 0x0100,
    AcceptLargeDlc = 0x0200,
    CanFd = 0x0400,
    CanFdNonIso = 0x0800,
}

/// CAN channel bitrates. Prefix '_' is for regular CAN whilst 'Fd' is for CAN-FD.
#[derive(Copy, Clone)]
pub enum CanBitrate {
    _1M     = -1,
    _500K   = -2,
    _250K   = -3,
    _125K   = -4,
    _100K   = -5,
    _62K    = -6,
    _50K    = -7,
    _83K    = -8,
    _10K    = -9,
    Fd500K  = -1000,
    Fd1M    = -1001,
    Fd2M    = -1002,
    Fd4M    = -1003,
    Fd8M    = -1004,
}

/// Message flags to be used with can_write() and returns from can_read().
#[derive(Copy, Clone)]
pub enum CanMsg {
    Mask    = 0x00FF,
    Rtr     = 0x0001,
    Std     = 0x0002,
    Ext     = 0x0004,
    Wakeup  = 0x0008,
    Nerr    = 0x0010,
    ErrorFrame = 0x0020,
    TxAck   = 0x0040,
    TxRq    = 0x0080,
    DelayMsg = 0x0100,
    SingleShot = 0x1000000,
    TxNack  = 0x2000000,
    MsgAbl  = 0x4000000,
}

/// Message flags (CAN FD) to be used with can_write() and returns from can_read().
#[derive(Copy, Clone)]
pub enum CanFdMsg {
    Fdf = 0x010000,
    Brs = 0x020000,
    Esi = 0x040000,
}

/// CAN statuses. Includes different error types
/// that CANlib might return after a function call. 
#[derive(Copy, Clone, PartialEq)]
pub enum CanStatus {
    CanOk               = 0,
    ErrParam            = -1,
    ErrNoMsg            = -2,
    ErrNotFound         = -3,
    ErrNoMem            = -4,
    ErrNoChannels       = -5,
    ErrInterrupted      = -6,
    ErrTimeout          = -7,
    ErrNotInitialized   = -8,
    ErrNoHandles        = -9,
    ErrInvHandle        = -10,
    ErrIniFile          = -11,
    ErrDriver           = -12,
    ErrTxBufOfl         = -13,
    ErrReserved1        = -14,
    ErrHardware         = -15,
    ErrDynaLoad         = -16,
    ErrDynaLib          = -17,
    ErrDynaInit         = -18,
    ErrNotSupported     = -19,
    ErrReserved5        = -20, 
    ErrReserved6        = -21, 
    ErrReserved2        = -22, 
    ErrDriverLoad       = -23,
    ErrDriverFailed     = -24,
    ErrNoConfigMgr      = -25,
    ErrNoCard           = -26,
    ErrReserved7        = -27, 
    ErrRegistry         = -28,
    ErrLicense          = -29,
    ErrInternal         = -30,
    ErrNoAccess         = -31,
    ErrNotImplemented   = -32,
    ErrDeviceFile       = -33,
    ErrHostFile         = -34,
    ErrDisk             = -35,
    ErrCrc              = -36,
    ErrConfig           = -37,
    ErrMemoFail         = -38,
    ErrScriptFail       = -39,
    ErrScriptWrongVersion = -40,
    ErrScriptTxeContainerVersion = -41,
    ErrScriptTxeContainerFormat = -42,
    ErrBufferTooSmall   = -43,
    ErrIoWrongPinType   = -44,
    ErrIoNotConfirmed   = -45,
    ErrIoConfigChanged  = -46,
    ErrIoPending        = -47,
    ErrIoNoValidConfig  = -48,
    ErrReserved         = -49,
}

/// Returns a CanStatus from an i32. It's not the prettiest thing,
/// but it's the easiest way to construct an enum from an integer.
fn i32_to_can_status(num: i32) -> CanStatus {
    match num {
        0 => CanStatus::CanOk,
        -1 => CanStatus::ErrParam,
        -2 => CanStatus::ErrNoMsg,       
        -3 => CanStatus::ErrNotFound,      
        -4 => CanStatus::ErrNoMem,          
        -5 => CanStatus::ErrNoChannels,        
        -6 => CanStatus::ErrInterrupted,   
        -7 => CanStatus::ErrTimeout,          
        -8 => CanStatus::ErrNotInitialized,   
        -9 => CanStatus::ErrNoHandles,        
        -10 => CanStatus::ErrInvHandle,        
        -11 => CanStatus::ErrIniFile,          
        -12 => CanStatus::ErrDriver,           
        -13 => CanStatus::ErrTxBufOfl,       
        -14 => CanStatus::ErrReserved1,        
        -15 => CanStatus::ErrHardware,       
        -16 => CanStatus::ErrDynaLoad,        
        -17 => CanStatus::ErrDynaLib,         
        -18 => CanStatus::ErrDynaInit,        
        -19 => CanStatus::ErrNotSupported,     
        -20 => CanStatus::ErrReserved5,         
        -21 => CanStatus::ErrReserved6,      
        -22 => CanStatus::ErrReserved2,      
        -23 => CanStatus::ErrDriverLoad,      
        -24 => CanStatus::ErrDriverFailed,    
        -25 => CanStatus::ErrNoConfigMgr,    
        -26 => CanStatus::ErrNoCard,          
        -27 => CanStatus::ErrReserved7,     
        -28 => CanStatus::ErrRegistry,      
        -29 => CanStatus::ErrLicense,       
        -30 => CanStatus::ErrInternal,       
        -31 => CanStatus::ErrNoAccess,          
        -32 => CanStatus::ErrNotImplemented,   
        -33 => CanStatus::ErrDeviceFile,       
        -34 => CanStatus::ErrHostFile,        
        -35 => CanStatus::ErrDisk,           
        -36 => CanStatus::ErrCrc,              
        -37 => CanStatus::ErrConfig,         
        -38 => CanStatus::ErrMemoFail,       
        -39 => CanStatus::ErrScriptFail,   
        -40 => CanStatus::ErrScriptWrongVersion,
        -41 => CanStatus::ErrScriptTxeContainerVersion,  
        -42 => CanStatus::ErrScriptTxeContainerFormat,  
        -43 => CanStatus::ErrBufferTooSmall, 
        -44 => CanStatus::ErrIoWrongPinType, 
        -45 => CanStatus::ErrIoNotConfirmed,  
        -46 => CanStatus::ErrIoConfigChanged, 
        -47 => CanStatus::ErrIoPending,       
        -48 => CanStatus::ErrIoNoValidConfig,  
        -49 => CanStatus::ErrReserved,       
        _   => unreachable!(), // Technically unreachable (?)
    }
}


/*
** CAN initialization
*/

/// Initialize the CANlib library.
pub fn can_initialize_library() {
    unsafe {
        canInitializeLibrary();
    }
}

/// Unloads the CANlib library. 
pub fn can_unload_library() {
    unsafe {
        canUnloadLibrary();
    }
}


/*
* CAN channel initialization
*/

/// Opens a new channel on the CAN bus through the adapter. Use for both normal CAN and CAN-FD.  
/// ## Arguments  
/// * `channel_no` - The channel to open. Most adapters support 1 or 2 channels at most.
/// * `channel_flags` - Settings for the CAN channel. See __CanOpen__.
/// 
/// ## Returns  
/// * `CanHandle` - A handle to the channel that's opened.
/// 
pub fn can_open_channel(channel_no: i32, channel_flags: i32) -> CanHandle {
    let handle;
    unsafe {
      handle = canOpenChannel(channel_no as c_int, channel_flags as c_int);
    }
    handle
}

/// Set the bus parameters on an opened CAN channel.  
/// ## Arguments  
/// * `hnd` - The handle to set the bus parameters on.   
/// * `freq` - The bitrate for this channel. See __CanBitrate__.  
/// * `tseg1` - Time/phase segment 1.   
/// * `tseg2` - Time/phase segment 2.     
/// * `sjw` - Synchronization jump width.   
/// * `no_samp` - Sampling points.
/// 
/// ## Returns  
/// * `CanStatus` - Status message. See __CanStatus__.
pub fn can_set_bus_params(hnd: CanHandle, freq: CanBitrate, tseg1: u32, tseg2: u32, sjw: u32, no_samp: u32) -> CanStatus {
    let status;
    unsafe {
        status = canSetBusParams(hnd, freq as c_long, tseg1 as c_uint, tseg2 as c_uint, sjw as c_uint, no_samp as c_uint, 0) as i32;
    }
    i32_to_can_status(status)
}

/// Set the bus parameters on an opened CAN-FD channel.  
/// ## Arguments  
/// * `hnd` - The handle to set the bus parameters on.  
/// * `freq_brs` - The bitrate for this channel. See __CanBitrate__.  
/// * `tseg1_brs` - Time/phase segment 1.  
/// * `tseg2_brs` - Time/phase segment 2.  
/// * `sjw_brs` Syncronization jump width.  
/// 
/// ## Returns  
/// * `CanStatus` - Status message. See __CanStatus__.
pub fn canfd_set_bus_params(hnd: CanHandle, freq_brs: CanBitrate, tseg1_brs: u32, tseg2_brs: u32, sjw_brs: u32) -> CanStatus {
    let status;
    unsafe {
        status = canSetBusParamsFd(hnd, freq_brs as c_long, tseg1_brs as c_uint, tseg2_brs as c_uint, sjw_brs as c_uint) as i32;
    }
    i32_to_can_status(status)
}

/// Set the driver type on an opened CAN channel.
/// ## Arguments  
/// * `hnd` - The handle to set the driver type on.  
/// * `driver_type` - The driver type. See __CanOpen__.  
/// 
/// ## Returns  
/// * `CanStatus` - Status message. See __CanStatus__.
pub fn can_set_bus_output_control(hnd: CanHandle, driver_type: CanDriver) -> CanStatus {
    let status;
    unsafe {
        status = canSetBusOutputControl(hnd, driver_type as u32) as i32;
    }
    i32_to_can_status(status)
}

/// Turns the bus on an opened CAN channel.
/// ## Arguments  
/// * `hnd` - The handle to turn on the bus on.  
/// 
/// ## Returns  
/// * `CanStatus` - Status message. See __CanStatus__.
pub fn can_bus_on(hnd: CanHandle) -> CanStatus {
    let status;
    unsafe {
        status = canBusOn(hnd) as i32;
    }
    i32_to_can_status(status)
}

/// Turns the bus off on an opened CAN channel.  
/// ## Arguments  
/// * `hnd` - The hande to turn off the bus on.  
/// 
/// ## Returns  
/// * `CanStatus` - Status message. See __CanStatus__.
pub fn can_bus_off(hnd: CanHandle) -> CanStatus {
    let status;
    unsafe {
        status = canBusOff(hnd) as i32;
    }    
    i32_to_can_status(status)
}

/// Closes a CAN channel on the specified handle.  
/// ## Arguments 
/// * `hnd` - The handle on which channel to close.  
/// 
/// ## Returns  
/// * `CanStatus` - Status message. See __CanStatus__.
pub fn can_close(hnd: CanHandle) -> CanStatus {
    let status;
    unsafe {
        status = canClose(hnd) as i32;
    }
    i32_to_can_status(status)
}


/*
** CAN write
*/

/// Writes a message from a buffer to the CAN channel on a specified handle. 
/// The message needs to be placed in a [u8] with the maximum size of 64 bytes.  
/// ## Arguments  
/// * `hnd` - The handle to write on.  
/// * `id` - CAN identifier.    
/// * `msg_buffer` - Unique reference to the buffer with the message.
/// * `dlc` - Length of the message (in bytes).  
/// * `flag` - Message flags. See __CanMsg__ and __CanFdMsg__.
/// 
/// ## Returns  
/// * `CanStatus` - Status message. See __CanStatus__.
pub fn can_write(hnd: CanHandle, id: i32, msg_buffer: &mut [u8], dlc: u32, flag: u32) -> CanStatus {
    let msg_bfr_ptr: *mut c_void = msg_buffer as *mut _ as *mut c_void;            
    let status;
    unsafe {
        status = canWrite(hnd, id as c_long, msg_bfr_ptr, dlc as c_uint, flag as c_uint) as i32;
    }
    i32_to_can_status(status)
}
/// Waits until all messages queued to send through the specified handle's channel are sent.  
/// Or until the given timeout period expires. Whichever happens first.  
/// ## Arguments
/// * `hnd` - The handle to wait on.  
/// * `timeout` - The time in milliseconds to wait for. 
/// 
/// ## Returns  
/// * `CanStatus` - Status message. See __CanStatus__.
pub fn can_write_sync(hnd: CanHandle, timeout: u32) -> CanStatus {
    let status;
    unsafe {
        status = canWriteSync(hnd, timeout as c_ulong) as i32;
    }
    i32_to_can_status(status)
}

/*
** CAN read
*/

/// Reads a message that's been recieved through the specified handle's CAN channel. If no message in buffer
/// will return a CanStatus error. 
/// ## Arguments
/// * `hnd` - The handle to read on.  
/// * `id` - Pointer to variable to write message ID to.  
/// * `msg_buffer` - Reference to buffer to write to.  
/// * `dlc` - Pointer to variable to write message DLC to.  
/// * `flag` - Pointer to variable to write message Flags to.
/// * `time` - Pointer to variable to write message time stamp.
/// 
/// ## Returns  
/// * `CanStatus` - Status message. See __CanStatus__.
pub fn can_read(hnd: CanHandle, id: *mut i32, msg_buffer: &mut [u8], dlc: *mut u32, flag: *mut u32, time: *mut u32) -> CanStatus {
    let msg_bfr_ptr: *mut c_void = msg_buffer as *mut _ as *mut c_void; 
    let status;
    unsafe{
        status = canRead(hnd, id as *mut c_long, msg_bfr_ptr, dlc as *mut c_uint, flag as *mut c_uint, time as *mut c_ulong) as i32;   
    }
    i32_to_can_status(status)
}

/// Waits for a message to arrive on the specified handle's CAN channel. Or
/// until the given timeout expires. Whichever happens first.  
/// ## Arguments  
/// * `hnd` - The handle to wait on.  
/// * `timeout` - The time in milliseconds to wait for.  
/// 
/// ## Returns  
/// * `CanStatus` - Status message. See __CanStatus__.
pub fn can_read_sync(hnd: CanHandle, timeout: u32) -> CanStatus {
    let status;
    unsafe {
        status = canReadSync(hnd, timeout as c_ulong) as i32;
    }
    i32_to_can_status(status)
}
